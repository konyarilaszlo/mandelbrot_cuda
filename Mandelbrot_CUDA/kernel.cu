
#include "cuda_runtime.h"
#include "device_launch_parameters.h"


#include <stdio.h>
#include <time.h>

extern "C" {
#include "bmp.h"
}


__global__ void CalculateMandelbrotSet(char* out, int width, int height) {
	unsigned int col = blockIdx.x * blockDim.x + threadIdx.x;
	unsigned int row = blockIdx.y * blockDim.y + threadIdx.y;
	int index = 3 * width * row + col * 3;

	double c_re = ((float)col - width / 2.0) * (4.0 / width);
	double c_im = ((float)row - height / 2.0) * (4.0 / width);

	double x = 0.0;
	double y = 0.0;

	int iteration = 0;
	int max_iteration = 1000;
	while (x * x + y * y <= 4 && iteration < max_iteration) {
		double x_new = x * x - y * y + c_re;
		y = 2 * x * y + c_im;
		x = x_new;
		iteration++;
	}

	if (iteration == max_iteration) {
		out[index] = 0;
		out[index + 1] = 0;
		out[index + 2] = 0;
	}
	else {
		int color = 255 - (iteration * 255 / max_iteration);
		out[index] = 50;
		out[index + 1] = 50;
		out[index + 2] = color;
	}
}

void renderMandelbrotSetGPU(int width, int height)
{
	size_t buffer_size = sizeof(char) * width * height * 3;

	char* image;
	cudaMalloc((void**)&image, buffer_size);

	char* host_image = (char*)malloc(buffer_size);

	dim3 blockDim(16, 16, 1);
	dim3 gridDim(width / blockDim.x, height / blockDim.y, 1);
	CalculateMandelbrotSet << < gridDim, blockDim, 0 >> > (image, width, height);

	cudaMemcpy(host_image, image, buffer_size, cudaMemcpyDeviceToHost);

	write_bmp("MandelbrotExport.bmp", width, height, host_image);

	cudaFree(image);
	free(host_image);
}

void renderMandelbrotSetCPU(int width, int height)
{
	size_t buffer_size = sizeof(char) * width * height * 3;

	char* host_image = (char*)malloc(buffer_size);

	for (int i = 0; i < width; i++)
	{
		for (int j = 0; j < height; j++)
		{
			int index = 3 * width * j + i * 3;

			double c_re = ((float)i - width / 2.0) * (4.0 / width);
			double c_im = ((float)j - height / 2.0) * (4.0 / width);

			double x = 0.0;
			double y = 0.0;

			int iteration = 0;
			int max_iteration = 1000;
			while (x * x + y * y <= 4 && iteration < max_iteration) {
				double x_new = x * x - y * y + c_re;
				y = 2 * x * y + c_im;
				x = x_new;
				iteration++;
			}

			if (iteration == max_iteration) {
				host_image[index] = 0;
				host_image[index + 1] = 0;
				host_image[index + 2] = 0;
			}
			else {
				int color = 255 - (iteration * 255 / max_iteration);
				host_image[index] = 50;
				host_image[index + 1] = 50;
				host_image[index + 2] = color;
			}
		}
	}

	write_bmp("MandelbrotCPUExport.bmp", width, height, host_image);

	free(host_image);
}

int main()
{
	clock_t StartTime = clock();
	renderMandelbrotSetGPU(4000, 3000);
	printf("Time taken on GPU: %.3fs\n", (double)(clock() - StartTime) / CLOCKS_PER_SEC);

	StartTime = clock();
	renderMandelbrotSetCPU(4000, 3000);
	printf("Time take on CPUn: %.3fs\n", (double)(clock() - StartTime) / CLOCKS_PER_SEC);

	return 0;
}
